#include <iostream>
#include <array>
#include <omp.h>
#include <math.h>

#define SIZE 8

std::array<float, SIZE> generate_random_array() {
    std::array<float, SIZE> a;
    for (size_t i = 0; i < SIZE; i++) {
        a[i] = rand();
    }
    return a;
}

std::array<float,  prefix_sum_1D(std::array arr) {
    int n = arr.size();
    int n_threads = n/log10(n);
    #pragma omp parralel num_threads(n)
    {

    }
}

int main(int argc, char const *argv[]) {
    std::cout << "Hello World" << '\n';
    std::cout << generate_random_array()[0] << '\n';
    return 0;
}
